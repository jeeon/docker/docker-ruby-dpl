#
# Install DPL
#

ENV DPL_VERSION=1.8.47

RUN gem install dpl --version $DPL_VERSION
