# docker-ruby-dpl

Ready-made docker image for deploying using **DPL**.

Very specific versions (some non-latest) are used.

- ruby
    - @ 2-stretch *(latest)*
    - @ 2-alpine
- gem
    - dpl @ 1.8.47 *(prefer latest 1.8.x)*

### Pull from Docker Hub
```
docker pull jeeon/ruby-dpl:2-default
docker pull jeeon/ruby-dpl:2-alpine
```

### Build from GitLab
```
docker build -t jeeon/ruby-dpl:2-default gitlab.com/jeeon/docker/ruby-dpl/2-default
docker build -t jeeon/ruby-dpl:2-alpine gitlab.com/jeeon/docker/ruby-dpl/2-alpine
```

### Run image
```
docker run -it jeeon/ruby-dpl:2-default bash
docker run -it jeeon/ruby-dpl:2-alpine sh
```

### Use as base image
```Dockerfile
FROM jeeon/ruby-dpl:2-default
FROM jeeon/ruby-dpl:2-alpine
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
