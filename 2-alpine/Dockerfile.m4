#
# Use standard Ruby base image
#

FROM ruby:2-alpine

include(common/maintainer.dockerfile)

include(common/install-dpl.dockerfile)
