#
# Use standard Ruby base image
#

FROM ruby:2-stretch

include(common/maintainer.dockerfile)

include(common/install-dpl.dockerfile)
