# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2019-03-01

### Changed

- Make code more DRY by using `m4` to generate `Dockerfile`s from common snippets.

## 2019-01-05

### Changed

- Fresh rebuild to get latest system updates and renew latest android sdk licences.

## 2018-04-04

### Changed

- Downgrade dpl to v1.8.47 (because v1.9.x is broken).
- Update docs about preferred dependency versions.

## 2018-04-01

### Changed

- Start keeping a ChangeLog.
- No changes to dependency versions.
